<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::recorce('members','MemberController');

Route::post('api/add_member','MemberController@store');
Route::post('api/edit_member','MemberController@update');
Route::post('api/delete_member','MemberController@destroy');
Route::post('api/send_notification','MemberController@notify');
